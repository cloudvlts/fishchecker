package org.shouthost.fishcheck.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import org.shouthost.fishcheck.commands.CommandCheck;

@Mod(name="FishChecker",modid="fishchecker",version="0.01")
public class FishCheck {
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event){

    }

    @Mod.EventHandler
    public void ServerStarting(FMLServerStartingEvent event){
        event.registerServerCommand(new CommandCheck());
    }

}
